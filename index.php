<?php
    $r = [
	    'docs'			=> 'https://docs.masterpoll.xyz',
	    'uptime'		=> 'https://uptime.masterpoll.xyz',
	    'discord-rules'	=> 'https://web.masterpoll.xyz/discord-rules',
	    'terms-conditions'=> 'https://web.masterpoll.xyz/terms-and-conditions',
	    'privacy-policy'=> 'https://web.masterpoll.xyz/privacy-policy',
	    'lbv-uptime'	=> 'https://uptime.lbvprojects.cf/?ref=mpred',
	    'june-prices'	=> 'https://t.me/s/MasterPoll/141',
	    'website'		=> 'https://masterpoll.xyz',
	    'blog'			=> 'https://masterpoll.xyz/blog',
	    'twitter'		=> 'https://twitter.com/MasterPollBot',
	    #'instagram'	=> 'https://instagr.am/#',
	    'tg-bots' 		=> 'https://t.me/MasterPollBots',
	    'tg-channel'	=> 'https://t.me/s/MasterPoll',
	    'tg-blog'		=> 'https://t.me/s/MasterPollBlog',
	    'tg-group'		=> 'https://t.me/MasterPollChat',
	    'tg-support'	=> 'https://t.me/MasterPollSupport',
	    'tg-pro'		=> 'https://t.me/MasterPollPremium',
		'tg-faq'		=> 'https://telegra.ph/MasterPoll-FAQ-12-24'
    ];

    if (!array_key_exists($_GET['p'], $r)){
        header('Location: https://masterpoll.xyz/404');
        exit();
    }

    header('Location: ' . $r[$_GET['p']]);
?>